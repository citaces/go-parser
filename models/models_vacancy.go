package models

type Vacancy struct {
	ID           int    `db:"id" json:"id"`
	Context      string `db:"context" json:"@context"`
	DatePosted   string `db:"date_posted" json:"datePosted"`
	Title        string `db:"title" json:"title"`
	Description  string `db:"description" json:"description"`
	ValidThrough string `db:"valid_through" json:"validThrough"`
}
