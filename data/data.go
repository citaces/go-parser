package data

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/bson"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"gitlab.com/citaces/go-parser/repository"
	"gitlab.com/citaces/go-parser/service"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

type BD struct {
	Host     string
	Port     int
	User     string
	Password string
	Name     string
	Driver   string
}

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func NewDB() (*service.Serv, error) {
	dbType := os.Getenv("DB_TYPE")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	var dsn string
	var DbRaw *sqlx.DB

	switch dbType {
	case "postgres":
		dsn = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPassword, dbName)
		db, err := sqlx.Open("postgres", dsn)
		if err != nil {
			return nil, errors.New("error with open sqlx db")
		}
		err = db.Ping()
		if err != nil {
			panic(err)
		}
		fmt.Println("Successfully connected!")

		migrationsPath := "file://migrations"

		driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
		if err != nil {
			log.Fatal(err)
		}

		m, err := migrate.NewWithDatabaseInstance(migrationsPath, "postgres", driver)
		if err != nil {
			log.Fatal(err)
		}

		err = m.Up()
		if err != nil {
			if err == migrate.ErrNoChange {
				log.Println("No migrations to apply")
			} else {
				log.Fatal(err)
			}
		}

		log.Println("Migrations applied successfully")
		DbRaw = db
		return &service.Serv{RepositoryPostgres: repository.NewVacancyStoragePostgres(DbRaw), RepositoryMongo: nil}, nil

	case "mongo":
		dsn = fmt.Sprintf("mongodb://%s:%s@%s:%s/%s", dbUser, dbPassword, dbHost, dbPort, dbName)
		clientOptions := options.Client().ApplyURI(dsn).SetAuth(options.Credential{
			Username: dbUser,
			Password: dbPassword,
		})
		client, err := mongo.Connect(context.Background(), clientOptions)
		if err != nil {
			log.Fatal(err)
		}

		collection := client.Database(dbName).Collection("vacancies")
		indexModel := mongo.IndexModel{
			Keys: bson.M{"_id": 1},
		}
		_, err = collection.Indexes().CreateOne(context.Background(), indexModel)
		if err != nil {
			log.Fatal(err)
		}
		return &service.Serv{RepositoryPostgres: nil, RepositoryMongo: repository.NewVacancyStorageMongo(collection)}, nil
	default:
		return nil, errors.New("invalid db_type")
	}
}
