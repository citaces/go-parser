package service

import (
	"context"

	"gitlab.com/citaces/go-parser/models"
	"gitlab.com/citaces/go-parser/repository"
)

type Service interface {
	CreateDTO(ctx context.Context, dto models.Vacancy) error
	GetListDTO(ctx context.Context) ([]models.Vacancy, error)
	DeleteDTO(ctx context.Context, id int) error
	GetByIdDTO(ctx context.Context, id int) (models.Vacancy, error)
}

type Serv struct {
	RepositoryPostgres repository.VacancyStoragerPostgres
	RepositoryMongo    repository.VacancyStoragerMongo
}

func NewServ(repoPostgres repository.VacancyStoragerPostgres, repoMongo repository.VacancyStoragerMongo) *Serv {
	return &Serv{
		RepositoryPostgres: repoPostgres,
		RepositoryMongo:    repoMongo,
	}
}

func (s *Serv) CreateDTO(ctx context.Context, dto models.Vacancy) error {
	if s.RepositoryMongo == nil {
		return s.RepositoryPostgres.Create(ctx, dto)
	}
	return s.RepositoryMongo.CreateMongo(ctx, dto)
}

func (s *Serv) GetListDTO(ctx context.Context) ([]models.Vacancy, error) {
	if s.RepositoryMongo == nil {
		return s.RepositoryPostgres.GetList(ctx)
	}
	return s.RepositoryMongo.GetListMongo(ctx)
}

func (s *Serv) DeleteDTO(ctx context.Context, id int) error {
	if s.RepositoryMongo == nil {
		return s.RepositoryPostgres.Delete(ctx, id)
	}
	return s.RepositoryMongo.DeleteMongo(ctx, id)
}

func (s *Serv) GetByIdDTO(ctx context.Context, id int) (models.Vacancy, error) {
	if s.RepositoryMongo == nil {
		return s.RepositoryPostgres.GetByID(ctx, id)
	}
	return s.RepositoryMongo.GetByIDMongo(ctx, id)
}
