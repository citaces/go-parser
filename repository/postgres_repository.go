package repository

import (
	"context"
	"errors"

	"github.com/jmoiron/sqlx"

	"gitlab.com/citaces/go-parser/models"
)

type VacancyStoragerPostgres interface {
	Create(ctx context.Context, dto models.Vacancy) error
	Delete(ctx context.Context, id int) error
	GetList(ctx context.Context) ([]models.Vacancy, error)
	GetByID(ctx context.Context, id int) (models.Vacancy, error)
}

type VacancyStoragePostgres struct {
	db *sqlx.DB
}

func NewVacancyStoragePostgres(db *sqlx.DB) *VacancyStoragePostgres {
	return &VacancyStoragePostgres{db: db}
}

func (e *VacancyStoragePostgres) Create(ctx context.Context, dto models.Vacancy) error {
	_, err := e.db.ExecContext(ctx, "INSERT INTO vacancies (context, description, date_posted, title, valid_through) VALUES ($1, $2, $3, $4, $5)", dto.Context, dto.Description, dto.DatePosted, dto.Title, dto.ValidThrough)
	return err
}

func (e *VacancyStoragePostgres) Delete(ctx context.Context, id int) error {
	query := `DELETE FROM vacancies WHERE id = $1`
	result, err := e.db.ExecContext(ctx, query, id)
	if err != nil {
		return err
	}
	d, _ := result.RowsAffected()
	if d == 0 {
		return errors.New("wrong id")
	}
	return nil
}

func (e *VacancyStoragePostgres) GetList(ctx context.Context) ([]models.Vacancy, error) {
	query := "SELECT * FROM vacancies"
	rows, err := e.db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var vacancies []models.Vacancy
	for rows.Next() {
		vacancy := models.Vacancy{}
		err := rows.Scan(&vacancy.ID, &vacancy.Context, &vacancy.Description,
			&vacancy.DatePosted, &vacancy.Title, &vacancy.ValidThrough)
		if err != nil {
			return nil, err
		}
		vacancies = append(vacancies, vacancy)
	}
	if len(vacancies) == 0 {
		return []models.Vacancy{}, errors.New("database in empty")
	}
	return vacancies, nil
}

func (e *VacancyStoragePostgres) GetByID(ctx context.Context, id int) (models.Vacancy, error) {
	query := `SELECT * FROM vacancies WHERE id = $1`
	var vacancy models.Vacancy
	err := e.db.GetContext(ctx, &vacancy, query, id)
	if err != nil {
		return models.Vacancy{}, errors.New("wrong id")
	}
	return vacancy, nil
}
