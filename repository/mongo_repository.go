package repository

import (
	"context"
	"errors"
	"log"

	"go.mongodb.org/mongo-driver/mongo/options"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/citaces/go-parser/models"
)

type VacancyStoragerMongo interface {
	CreateMongo(ctx context.Context, dto models.Vacancy) error
	DeleteMongo(ctx context.Context, id int) error
	GetListMongo(ctx context.Context) ([]models.Vacancy, error)
	GetByIDMongo(ctx context.Context, id int) (models.Vacancy, error)
}

type VacancyStorageMongo struct {
	collection *mongo.Collection
}

func NewVacancyStorageMongo(collection *mongo.Collection) *VacancyStorageMongo {
	return &VacancyStorageMongo{collection: collection}
}

func getNextSequenceValue(ctx context.Context, coll *mongo.Collection) (int, error) {
	sequenceDocument := bson.M{}
	change := options.FindOneAndUpdate().SetUpsert(true).SetReturnDocument(options.After)
	filter := bson.M{"_id": "vacancy_id_seq"}
	update := bson.M{"$inc": bson.M{"seq": 1}}

	err := coll.FindOneAndUpdate(ctx, filter, update, change).Decode(&sequenceDocument)
	if err != nil {
		log.Fatal(err)
	}
	return int(sequenceDocument["seq"].(int32)), nil
}

func (e *VacancyStorageMongo) CreateMongo(ctx context.Context, vacancy models.Vacancy) error {
	vacancy.ID, _ = getNextSequenceValue(ctx, e.collection)
	_, err := e.collection.InsertOne(ctx, vacancy)
	if err != nil {
		return err
	}
	return nil
}

func (e *VacancyStorageMongo) DeleteMongo(ctx context.Context, id int) error {
	result, err := e.collection.DeleteOne(ctx, bson.M{"id": id})
	if err != nil {
		return err
	}
	if result.DeletedCount == 0 {
		return errors.New("wrong id")
	}
	return nil
}

func (e *VacancyStorageMongo) GetListMongo(ctx context.Context) ([]models.Vacancy, error) {
	var vacancies []models.Vacancy
	cur, err := e.collection.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)
	for cur.Next(ctx) {
		var vacancy models.Vacancy
		err := cur.Decode(&vacancy)
		if err != nil {
			return nil, err
		}
		vacancies = append(vacancies, vacancy)
	}
	if err := cur.Err(); err != nil {
		return nil, err
	}
	if len(vacancies) == 0 {
		return []models.Vacancy{}, errors.New("database in empty")
	}
	return vacancies[1:], nil
}

func (e *VacancyStorageMongo) GetByIDMongo(ctx context.Context, id int) (models.Vacancy, error) {
	var vacancy models.Vacancy
	err := e.collection.FindOne(ctx, bson.M{"id": id}).Decode(&vacancy)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return vacancy, errors.New("wrong id")
		}
		return vacancy, err
	}
	return vacancy, nil
}
