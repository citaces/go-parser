package main

import (
	"gitlab.com/citaces/go-parser/controllers"
	"gitlab.com/citaces/go-parser/data"
	"gitlab.com/citaces/go-parser/handlers"
)

func main() {
	serv, err := data.NewDB()
	if err != nil {
		panic(err)
	}
	control := controllers.NewVacControl(serv)
	handler := handlers.NewHandlers(control)
	handler.Route()
}
