package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/citaces/go-parser/selenium"
	"gitlab.com/citaces/go-parser/service"
)

type VacancyController interface {
	CreateVacancies(w http.ResponseWriter, r *http.Request)
	ListVacancies(w http.ResponseWriter, r *http.Request)
	DeleteVacancy(w http.ResponseWriter, r *http.Request)
	GetVacancy(w http.ResponseWriter, r *http.Request)
}

type VacancyControl struct {
	service service.Service
}

func NewVacControl(service service.Service) *VacancyControl {
	return &VacancyControl{service}
}

func (u *VacancyControl) CreateVacancies(w http.ResponseWriter, r *http.Request) {
	category := chi.URLParam(r, "category")
	s := selenium.NewSelenium(category)
	res, err := s.Run()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	for _, vac := range res {
		err = u.service.CreateDTO(ctx, vac)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}
	_, err = fmt.Fprintf(w, "successful, 200 OK!")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *VacancyControl) ListVacancies(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	vacancies, err := u.service.GetListDTO(ctx)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vacancies)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *VacancyControl) DeleteVacancy(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	id := chi.URLParam(r, "id")
	n, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = u.service.DeleteDTO(ctx, n)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	_, err = fmt.Fprintf(w, "successful, 200 OK!")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *VacancyControl) GetVacancy(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	id := chi.URLParam(r, "id")
	n, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	vacancy, err := u.service.GetByIdDTO(ctx, n)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
