package selenium

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/citaces/go-parser/models"

	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
)

type Selenium struct {
	Res         []models.Vacancy
	Category    string
	BrowserName string
	Dd          *goquery.Selection
	Wd          selenium.WebDriver
	MaxTries    int
	DriverPort  int
	MainUrl     string
	UrlPrefix   string
	Url         string
}

func NewSelenium(category string) *Selenium {
	return &Selenium{
		Res:         make([]models.Vacancy, 0),
		Category:    category,
		BrowserName: "firefox",
		MaxTries:    5,
		DriverPort:  4444,
		MainUrl:     "https://career.habr.com",
		UrlPrefix:   "http://selenium:%d/wd/hub",
		Url:         "https://career.habr.com/vacancies?page=%d&q=%s&type=all",
	}
}

func (s *Selenium) Run() ([]models.Vacancy, error) {
	caps := selenium.Capabilities{
		"browserName": s.BrowserName,
	}
	var err error
	for i := 0; i < s.MaxTries; i++ {
		s.Wd, err = selenium.NewRemote(caps, fmt.Sprintf(s.UrlPrefix, s.DriverPort))
		if err == nil {
			break
		}
		log.Printf("Error connecting to remote driver: %v", err)
		time.Sleep(5 * time.Second)
	}
	if s.Wd == nil {
		log.Fatalf("Failed to connect to remote driver after %d attempts", s.MaxTries)
	}

	defer func(Wd selenium.WebDriver) {
		err := Wd.Quit()
		if err != nil {
			log.Fatalf("Failed to quit")

		}
	}(s.Wd)

	page := 1
	err = s.Wd.Get(fmt.Sprintf(s.Url, page, s.Category))
	if err != nil {
		return []models.Vacancy{}, err
	}
	elem, err := s.Wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return []models.Vacancy{}, err
	}
	vacancyCountRaw, err := elem.Text()
	if err != nil {
		return []models.Vacancy{}, err
	}
	pages, err := strconv.Atoi(strings.Split(vacancyCountRaw, " ")[1])
	if err != nil {
		return []models.Vacancy{}, err
	}

	for i := 0; i <= pages/25; i++ {
		elems, err := s.Wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			return []models.Vacancy{}, err
		}
		var links []string
		for i := range elems {
			var link string
			link, err = elems[i].GetAttribute("href")
			if err != nil {
				continue
			}
			links = append(links, s.MainUrl+link)
		}
		for _, link := range links {
			resp, err := http.Get(link)
			if err != nil {
				return []models.Vacancy{}, err
			}
			var doc *goquery.Document
			doc, err = goquery.NewDocumentFromReader(resp.Body)
			if err != nil && doc != nil {
				return []models.Vacancy{}, err
			}
			s.Dd = doc.Find("script[type=\"application/ld+json\"]")
			if s.Dd == nil {
				return []models.Vacancy{}, errors.New("habr vacancy nodes not found")
			}
			var vac models.Vacancy
			b := bytes.NewBufferString(s.Dd.First().Text())
			_ = json.NewDecoder(b).Decode(&vac)
			s.Res = append(s.Res, vac)
		}
		if i == pages/25 {
			break
		}
		page++
		err = s.Wd.Get(fmt.Sprintf(s.Url, page, s.Category))
		if err != nil {
			log.Println("error when trying to navigate to url")
		}
	}
	return s.Res, nil
}
