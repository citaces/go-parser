CREATE TABLE IF NOT EXISTS vacancies (
                                         id SERIAL PRIMARY KEY,
                                         context VARCHAR(255),
                                         description VARCHAR(12000),
                                         date_posted VARCHAR(255),
                                         title VARCHAR(255),
                                         valid_through VARCHAR(255)
)